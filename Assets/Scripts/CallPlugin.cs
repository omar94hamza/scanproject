﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallPlugin : MonoBehaviour {

    private String xAppToken = "a~rR-*Zd9t~H8wm@CmPd"; //given from the backend
    private String xUserToken = "6S-NKAAJpv22-gx_TfWW"; //comes from using the Session API https://nasnav.com//ipa-employee/ver/sion/one/session.json with email and password

	// Use this for initialization
	void Start () {
        //to get the activity context to pass it to the plugin
		AndroidJavaClass jcContext = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
	    AndroidJavaObject joContext = jcContext.GetStatic<AndroidJavaObject>("currentActivity");

        //accessing the class to call a static method (plugin)
        AndroidJavaClass jcCall = new AndroidJavaClass("com.nasnav.scanlibrary.LibHelper");

        //Calling the CallScan Method to which the current activity is passed
        jcCall.CallStatic("CallScan", joContext, xAppToken, xUserToken);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
